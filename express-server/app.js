var express = require("express"), cors = require('cors');
var app = express();
app.use(express.json());
app.use(cors());
app.listen(3000,()=> console.log("Servidor run por el puerto 3000"));

var noticias = ["sabanas", "Cobijas","toallas","cubrelechos","demas"];

app.get("/get",(req, res,next)=>
res.json(noticias.filter((c)=> c.toLowerCase().indexOf(
    req.query.q.toString().toLowerCase()) > -1)));

var misFavoritos= [];
app.get("/favs",(req, res, next)=> res.json(misFavoritos));
app.post("/favs", (req, res, next)=>{
    console.log(req.body);
    misFavoritos.push(req.body.nuevo);
    res.json(misFavoritos);
});

// app.get("/api/translation", (req, res, next) => res.json([
//     {lang: req.query.lang, key: 'HOLA', value: 'HOLA ' + req.query.lang}
//   ]));