import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { compose } from "nativescript-email"; 
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, Color, View } from "@nativescript/core";
import { NoticiasServices } from "../domain/noticias.service";
import { RouterExtensions } from "@nativescript/angular";
import * as SocialShare from 'nativescript-social-share';
import * as Toast from "nativescript-toasts";
import { AppState } from "../app.module";
import { Store } from "@ngrx/store";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
const fs = require("file-system");  
const appFolder = fs.knownFolders.currentApp();   
const appPath = appFolder.path;   
const logoPath = appPath + "/app/res/icon.png"; 

@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
  //  providers: [NoticiasServices]
})

export class SearchComponent implements OnInit {
  resultados: Array<string>;
  textFieldValue: string;
  @ViewChild("layout") layout: ElementRef;

    constructor(private noticias: NoticiasServices, 
      private routerExtensions: RouterExtensions,
      private store: Store<AppState>
      ) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
      this.store.select((state)=>  state.noticias.sugerida)
      .subscribe((data)=>{
        const f = data;
        if(f != null){
          Toast.show({text: "Sugerimos leer: "+f.titulo, duration: Toast.DURATION.SHORT});
        }
      });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
    onButtonTap(){
      this.noticias.agregar(this.textFieldValue).then((r: any)=>{
        console.log("resultados buscar ahora"+JSON.stringify(r));
        this.resultados  = r;  
        this.EnviarMensaje();    
      });
    }

    EnviarMensaje(){
      compose({
        subject: "Mail de Prueba",  
        body: "Hola <strong>mundo!</strong> :)",  
        to: ["mail@mail.com"],  
        cc: [],   
        bcc: [],  
        attachments: [  
            {
              fileName: "arrow1.png", 
              path:  "base64://iVBORw0KGgoAAAANSUhEUgAAABYAAAAoCAYAAAD6xArmAAAACXBIWXMAABYlAAAWJQFJUiTwAAAAHGlET1QAAAACAAAAAAAAABQAAAAoAAAAFAAAABQAAAB5EsHiAAAAAEVJREFUSA1iYKAimDhxYjwIU9FIBgaQgZMmTfoPwlOmTJGniuHIhlLNxaOGwiNqNEypkwlGk9RokoIUfaM5ijo5Clh9AAAAAP//ksWFvgAAAEFJREFUY5g4cWL8pEmT/oMwiM1ATTBqONbQHA2W0WDBGgJYBUdTy2iwYA0BrILDI7VMmTJFHqv3yBUEBQsIg/QDAJNpcv6v+k1ZAAAAAElFTkSuQmCC",
              mimeType: "image/png" 
            },
            {
              fileName: "icon.png", 
              path: logoPath,
              mimeType: "image/png"
    }]
    }).then(() => console.log("Enviador de mail cerrado"), (err) => console.log("Error: " + err)); 
   

    }

    onItemTap(x): void {
      console.dir(x);

    //   this.routerExtensions.navigate(["/DetalleSearch"], {
    //     transition: {
    //         name: "fade"
    //     }
    // });

       this.store.dispatch(new NuevaNoticiaAction(new Noticia(x.view.bindingContext)));

    }

    onLongPress(s): void{
      console.log(s);
      SocialShare.shareText(s, "Asunto: compartido desde el curso");


    }

    
    buscarAhora(s: string ) {

      console.dir("Buscar ahora"+s);
      this.noticias.buscar(s).then((r: any)=>{
        console.log("resultados buscar ahora"+JSON.stringify(r));
        this.resultados  = r;
      },(e) => {
        console.log("error buscar ahora"+e);
        Toast.show({ text: "Error en la busqueda", duration: Toast.DURATION.SHORT});
      });


   
 
       
  }
}
