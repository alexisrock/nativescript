import { Component, OnInit } from "@angular/core";
import * as camera from "nativescript-camera";

import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onButonTap(): void{
        camera.requestPermissions().then(
            function success(){
                const options = { width: 300, height: 300, KeepAspectRatio: false, savetoGallery: true};
                camera.takePicture(options).then(
                    (imageAsset)=>{


                        
                        console.log("tamaño"+imageAsset.options.width+ "x"+imageAsset.options.height);
                        console.log("KeepAspectRatio"+imageAsset.options.KeepAspectRatio);
                        console.log("Foto guardada");
                    }).catch((err)=>{
                        console.log("error"+err.message);
                    } );

            },
            function failure(){
                console.log("permiso de camara no aceptado");
            }
        );

    }
}
