import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";

import { EditarRoutingModule } from "./Editar-routing.module";
import { EditarComponent } from "./Editar.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        EditarRoutingModule
    ],
    declarations: [
        EditarComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class EditarModule { }
